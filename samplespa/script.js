	// create the module and name it cartApp
	var cartApp = angular.module('cartApp', ['ngRoute']);

	// configure our routes
	cartApp.config(function($routeProvider) {
		$routeProvider

			// route for the home page
			.when('/', {
				templateUrl : 'pages/home.html',
				controller  : 'formCtrl'
			})		
	});

	// create the controller and inject Angular's $scope
	cartApp.controller('formCtrl', function($scope) {
		$rootScope.master = {productName1 :"", productquantity1: 0, productprice1: 0};
		$scope.reset = function() {
			$scope.user = angular.copy($scope.master);
		};
		$scope.reset();
	});


	