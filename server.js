var http = require('http');
/*
function onRequest (request, response){
    response.writeHead(200, {'Content-Type': 'text/plain'});
    response.write('Hello World');
    response.end();
}

http.createServer(onRequest).listen(8000);
*/

function DBaccess (request,h) {
    return new Promise(function(resolve,reject){
    
        console.log(`Server running at: ${server.info.uri}`);    
        var {pools, Client} = require('pg');
        const client = new Client({
            user: 'postgres',
            host: 'localhost',
            database: 'toilettracker',
            password: 'abcd1234',
            port: 5432,
            })

        client.connect()
        
        //var querystring = "insert into toiloc (content) values ( '{\"name\":\"Jeba\", \"address\":\"Madurai\"}')"
        //var deletequery = "delete from toiloc where id>4"
        client.query('SELECT * from toiloc' , (err, res) => {
            if(err){
                console.log(err);
                reject(err);
            }
            else{
                console.log(res.rows);
                client.end()
                const response = h.response(res.rows);
                response.type('text/plain');
                resolve(response);
            }
        })
       
        }).then(function(response){
            return response;
        }).catch(function(err){
            const response = h.response('bad db connection');
            response.type('text/plain');
            return response;
        })
}

const Hapi = require('hapi');

const joi = require('joi');

const server = new Hapi.Server({ port: 8000, host: 'localhost' });


const start = async () => {

    await server.register(require('inert'));

    server.route({
        method: 'GET',
        path: '/',
        handler: function (request, h) {
            const response = h.response('hello world');
            response.type('text/plain');
            return response;
        }
    });
    server.route({
        method: 'GET',
        path: '/db',
        handler:function (request, h) {
                return DBaccess(request,h);
            }
        
    });

    server.route({
        method: 'POST',
        path: '/login',
        config:{
            handler:function (request, h) {
                return new Promise(function(resolve,reject){
                    var {pools, Client} = require('pg');
                    const client = new Client({
                        user: 'postgres',
                        host: 'localhost',
                        database: 'toilettracker',
                        password: 'abcd1234',
                        port: 5432,
                        })

                    client.connect()

                    var querystring = `SELECT userid, password FROM userinfo WHERE  userid = '${request.payload.email}'`;
                    client.query(querystring , (err, res) => {
                        if(err){
                            //console.log(err);
                            reject(err);
                        }
                        else{
                            client.end()
                            var response = h.response('Voila, User found');
                            
                            if(res.rowCount == 0){
                                response = h.response('USER NOT FOUND');
                            }
                            else if(res.rows[0].password == request.payload.password){
                                response = h.response('true')
                            }
                            else{
                                response = h.response('false')
                            }

                            response.type('text/plain');
                            resolve(response);
                        }
                    })
                   
                    }).then(function(response){
                        return response;
                    }).catch(function(err){
                        console.log(err);
                        const response = h.response('dont know');
                        response.type('text/plain');
                        return response;
                    })
                    
                },
            validate:{
                payload: joi.object({
                    email: joi.string().email().required(),
                    password : joi.string().required().min(8)
                }),
                failAction: function (request, reply, error) {
                    const response = reply.response(error.output.payload);
                    response.type('text/plain')
                    return response.takeover()       //Error: Lifecycle methods called before the handler can only return an error, a takeover response, or a continue signal
                }
            }
        }
    })

    server.route({
        method: 'POST',
        path: '/register',
        config:{
            handler:function (request, h) {
                return new Promise(function(resolve,reject){
                    var {pools, Client} = require('pg');
                    const client = new Client({
                        user: 'postgres',
                        host: 'localhost',
                        database: 'toilettracker',
                        password: 'abcd1234',
                        port: 5432,
                        })

                    client.connect()

                    var querystring = `insert into userinfo (userid,password,toiletid) values ( '${request.payload.email}', '${request.payload.password}', '[]')`;
                    client.query(querystring , (err, res) => {
                        if(err){
                            //console.log(err);
                            reject(err);
                        }
                        else{
                            //console.log(res.rows);
                            client.end()
                            const response = h.response('true');
                            response.type('text/plain');
                            resolve(response);
                        }
                    })
                   
                    }).then(function(response){
                        return response;
                    }).catch(function(err){
                        console.log("the info of error" + err.code);
                        var response
                        if(err.code == 23505){
                            response = h.response('false');
                        }
                        else{
                            response = h.response('UNKNOWN ERROR... SEE CONSOLE FOR MORE INFO');
                            console.log(err);   
                        }
                        response.type('text/plain');
                        return response;
                    })
                    
                },
            validate:{
                payload: joi.object({
                    email: joi.string().email().required(),
                    password : joi.string().required().min(8)
                }),
                failAction: function (request, reply, error) {
                    const response = reply.response(error.output.payload);
                    response.type('text/plain')
                    return response.takeover()       //Error: Lifecycle methods called before the handler can only return an error, a takeover response, or a continue signal
                }
            }
        }
    })

    server.route({
        method: 'POST',
        path: '/addtoilet',
        config:{
            handler:function (request, h) {
                return new Promise(function(resolve,reject){
                    var {pools, Client} = require('pg');
                    const client = new Client({
                        user: 'postgres',
                        host: 'localhost',
                        database: 'toilettracker',
                        password: 'abcd1234',
                        port: 5432,
                        })

                    client.connect()
                
                    var querystring = `INSERT INTO toiloc (content) VALUES ( '{ "updateruserid" : "${request.payload.updateruserid}",
                    "placename" : "${request.payload.placename}",
                    "address" : "${request.payload.address}",
                    "latitude" : "${request.payload.latitude}",
                    "longitude" : "${request.payload.longitude}",
                    "paid" : "${request.payload.paid}",
                    "ntmen" : "${request.payload.ntmen}",
                    "ntwomen" : "${request.payload.ntwomen}",
                    "reviews" : "${request.payload.reviews}",
                    "type" : "${request.payload.type}",
                    "inservice" : "${request.payload.inservice}"}') RETURNING tid`;     //Returns AUTO-INCREMENTED toilet id  


                    var autoIncTID
                    client.query(querystring , (err, res) => {
                        if(err){
                            console.log(err);
                            reject(err);
                        }
                        else{
                            autoIncTID =  res.rows[0].tid;
                            client.query(`UPDATE userinfo SET toiletid = toiletid || '["${autoIncTID}"]' :: jsonb WHERE userid = '${request.payload.updateruserid}' `,(err,res) =>{
                                if(err){
                                    console.log ("The inner query error " + err);
                                }
                                else{
                                    client.end()
                                }
                            })
                            var response = h.response('true');
                            response.type('text/plain');
                            resolve(response);
                        }
                    })
                    }).then(function(response){
                        return response;
                    }).catch(function(err){
                        console.log(err);
                        const response = h.response('false');
                        response.type('text/plain');
                        return response;
                    })
                    
                },
            validate:{
                payload: joi.object({
                    updateruserid: joi.string().email().optional(),
                    placename : joi.string().required(),
                    address : joi.string().required(),
                    latitude : joi.number().required(),
                    longitude : joi.number().required(),
                    paid : joi.number().required(),
                    ntmen : joi.number().required(),
                    ntwomen : joi.number().required(),
                    reviews : joi.array().optional(),
                    type: joi.string().valid('indian','western').required(),
                    inservice: joi.string().valid('Y','N','yes','no').required(),
                }),
                failAction: function (request, reply, error) { 
                    const response = reply.response(error.output.payload);
                    response.type('text/plain')
                    return response.takeover()       //Error: Lifecycle methods called before the handler can only return an error, a takeover response, or a continue signal
                }
            }
        }
    })

    server.route({
        method: 'POST',
        path: '/updatetoilet',
        config:{
            handler:function (request, h) {
                return new Promise(function(resolve,reject){
                    var {pools, Client} = require('pg');
                    const client = new Client({
                        user: 'postgres',
                        host: 'localhost',
                        database: 'toilettracker',
                        password: 'abcd1234',
                        port: 5432,
                        })

                    client.connect()
                
                    var querystring = `UPDATE toiloc SET content = jsonb_set (content, '{inservice}', '"${request.payload.inservice}"' ) where tid = '${request.payload.tid}'`; 
                    client.query(querystring , (err, res) => {
                        if(err){
                            console.log(err);
                            reject(err);
                        }
                        else{
                            var response = h.response('Toilet service status modified to ' + request.payload.tid + " " + request.payload.inservice);
                            response.type('text/plain');
                            resolve(response);
                        }
                    })
                    }).then(function(response){
                        return response;
                    }).catch(function(err){
                        console.log(err);
                        const response = h.response('dont know... Bad db connection');
                        response.type('text/plain');
                        return response;
                    })
                    
                },
            validate:{
                payload: joi.object({
                    tid: joi.number().required(),
                    inservice: joi.string().valid('Y','N','yes','no').required(),
                }),
                failAction: function (request, reply, error) { 
                    const response = reply.response(error.output.payload);
                    response.type('text/plain')
                    return response.takeover()       //Error: Lifecycle methods called before the handler can only return an error, a takeover response, or a continue signal
                }
            }
        }
    })

    server.route({
        method: 'POST',
        path: '/viewtoilet',
        config:{
            handler:function (request, h) {
                return new Promise(function(resolve,reject){
                    var {pools, Client} = require('pg');
                    const client = new Client({
                        user: 'postgres',
                        host: 'localhost',
                        database: 'toilettracker',
                        password: 'abcd1234',
                        port: 5432,
                        })

                    client.connect()
                
                    var querystring = `SELECT * FROM toiloc where content ->> 'address' ILIKE  '%${request.payload.location}%'`; 
                    client.query(querystring , (err, res) => {
                        if(err){
                            console.log(err);
                            reject(err);
                        }
                        else{
                            console.log(res.rows);
                            var response
                            if(res.rows.length==0){
                                response = h.response('No results found yet...')
                            }
                            else{
                                response = h.response(res.rows)
                            }
                            response.type('text/plain');
                            resolve(response);
                        }
                    })
                    }).then(function(response){
                        return response;
                    }).catch(function(err){
                        console.log(err);
                        const response = h.response('dont know... Bad db connection');
                        response.type('text/plain');
                        return response;
                    })
                    
                },
            validate:{
                payload: joi.object({
                    location: joi.string().required(),
                }),
                failAction: function (request, reply, error) { 
                    const response = reply.response(error.output.payload);
                    response.type('text/plain')
                    return response.takeover()       //Error: Lifecycle methods called before the handler can only return an error, a takeover response, or a continue signal
                }
            }
        }
    })

    server.route({
        method: 'POST',
        path: '/addreview',
        config:{
            handler:function (request, h) {
                return new Promise(function(resolve,reject){
                    var {pools, Client} = require('pg');
                    const client = new Client({
                        user: 'postgres',
                        host: 'localhost',
                        database: 'toilettracker',
                        password: 'abcd1234',
                        port: 5432,
                        })

                    client.connect()
                
                    var querystring = `UPDATE toiloc SET content = jsonb_set (content, '{reviews}', content->'reviews' || '["${request.payload.reviews}"]' ::jsonb) where tid = '${request.payload.tid}'`;
                    client.query(querystring , (err, res) => {
                        if(err){
                            console.log(err);
                            reject(err);
                        }
                        else{
                            console.log(res.rows);
                            var response = h.response('true')
                            response.type('text/plain');
                            resolve(response);
                        }
                    })
                    }).then(function(response){
                        return response;
                    }).catch(function(err){
                        console.log(err);
                        const response = h.response('dont know... Bad db connection');
                        response.type('text/plain');
                        return response;
                    })
                    
                },
            validate:{
                payload: joi.object({
                    tid: joi.number().required(),
                    reviews: joi.string().required()
                }),
                failAction: function (request, reply, error) { 
                    const response = reply.response(error.output.payload);
                    response.type('text/plain')
                    return response.takeover()       //Error: Lifecycle methods called before the handler can only return an error, a takeover response, or a continue signal
                }
            }
        }
    })

    server.ext('onPreResponse', (request, reply) => {
        
        const response = request.response;
        if (response.isBoom && (response.output.statusCode === 404)  ) {
            console.log('pre response check executed');
            return reply.file('./samplemap.html');
        }
        
        return reply.continue;
    });
    
    

    await server.start();

    console.log('Server running at:', server.info.uri);
};

start();